package ru.t1.panasyuk.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.panasyuk.tm.api.repository.model.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.model.ITaskRepository;
import ru.t1.panasyuk.tm.api.service.model.IProjectTaskService;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.exception.field.ProjectIdEmptyException;
import ru.t1.panasyuk.tm.exception.field.TaskIdEmptyException;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected IProjectRepository getProjectRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @NotNull
    protected ITaskRepository getTaskRepository() {
        return context.getBean(ITaskRepository.class);
    }

    @NotNull
    @Override
    public Task bindTaskToProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Task task;
        @Nullable final Project project;
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            project = projectRepository.findOneById(userId, projectId);
            boolean isExist = project != null;
            if (!isExist) throw new ProjectNotFoundException();
            task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProject(project);
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public Project removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable Project project;
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            project = projectRepository.findOneById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            projectRepository.remove(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public Project removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final Project project;
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            if (index > projectRepository.getSize(userId)) throw new IndexIncorrectException();
            project = projectRepository.findOneByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            removeProjectById(userId, project.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return project;
    }

    @Override
    public void clearProjects(@NotNull final String userId) {
        @Nullable final List<Project> projects;
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projects = projectRepository.findAll(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        if (projects == null) return;
        for (@NotNull final Project project : projects) removeProjectById(userId, project.getId());
    }

    @NotNull
    @Override
    public Task unbindTaskFromProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Task task;
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean isExist = projectRepository.findOneById(userId, projectId) != null;
            if (!isExist) throw new ProjectNotFoundException();
            task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProject(null);
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return task;
    }

}