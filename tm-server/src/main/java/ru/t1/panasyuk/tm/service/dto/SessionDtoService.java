package ru.t1.panasyuk.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.panasyuk.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.panasyuk.tm.api.service.dto.ISessionDtoService;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, ISessionDtoRepository>
        implements ISessionDtoService {

    @NotNull
    @Override
    protected ISessionDtoRepository getRepository() {
        return context.getBean(ISessionDtoRepository.class);
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll() {
        @Nullable final List<SessionDTO> sessions;
        @NotNull final ISessionDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            sessions = repository.findAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return sessions;
    }

    @NotNull
    @Override
    public SessionDTO remove(@Nullable final SessionDTO session) {
        if (session == null) throw new EntityNotFoundException();
        @NotNull final ISessionDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return session;
    }

}