package ru.t1.panasyuk.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.panasyuk.tm.api.repository.model.ISessionRepository;
import ru.t1.panasyuk.tm.api.service.model.ISessionService;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository>
        implements ISessionService {

    @NotNull
    @Override
    protected ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

    @Nullable
    @Override
    public List<Session> findAll() {
        @Nullable final List<Session> sessions;
        @NotNull final ISessionRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            sessions = repository.findAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return sessions;
    }

    @NotNull
    @Override
    public Session remove(@Nullable final Session session) {
        if (session == null) throw new EntityNotFoundException();
        @NotNull final ISessionRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return session;
    }

}