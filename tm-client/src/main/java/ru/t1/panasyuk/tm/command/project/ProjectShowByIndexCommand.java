package ru.t1.panasyuk.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.project.ProjectFindOneByIndexRequest;
import ru.t1.panasyuk.tm.dto.response.project.ProjectFindOneByIndexResponse;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Show project by index.";

    @NotNull
    private static final String NAME = "project-show-by-index";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final ProjectFindOneByIndexRequest request = new ProjectFindOneByIndexRequest(getToken(), index);
        @NotNull final ProjectFindOneByIndexResponse response = projectEndpoint.findProjectByIndex(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        @Nullable final ProjectDTO project = response.getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}