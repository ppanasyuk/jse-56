package ru.t1.panasyuk.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.data.DataYamlSaveFasterXMLRequest;

@Component
public final class DataYamlSaveFasterXMLCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Save data in yaml file.";

    @NotNull
    private static final String NAME = "data-save-yaml-fasterxml";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE YAML]");
        @NotNull final DataYamlSaveFasterXMLRequest request = new DataYamlSaveFasterXMLRequest(getToken());
        domainEndpoint.saveDataYamlFasterXML(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
