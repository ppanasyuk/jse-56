package ru.t1.panasyuk.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.data.DataBase64SaveRequest;

@Component
public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Save data to base64 file.";

    @NotNull
    private static final String NAME = "data-save-base64";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest(getToken());
        domainEndpoint.saveDataBase64(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}