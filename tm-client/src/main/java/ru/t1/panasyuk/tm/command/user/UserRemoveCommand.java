package ru.t1.panasyuk.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.user.UserRemoveRequest;
import ru.t1.panasyuk.tm.dto.response.user.UserRemoveResponse;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.util.TerminalUtil;

@Component
public final class UserRemoveCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-remove";

    @NotNull
    private final String DESCRIPTION = "Remove user";

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken(), login);
        @NotNull final UserRemoveResponse response = userEndpoint.removeUser(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}